from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm

# Create your views here.
def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    todo_items = todo_list.items.all()
    context = {
        "todo_list": todo_list,
        "todo_items": todo_items
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
            form = TodoListForm()

    context = {"form": form}
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)
        
    context = {
        "form": form,
        "todo_list": todo_list
    }
    return render(request, "todos/update.html", context)










# from django.shortcuts import render, get_object_or_404, redirect
# from recipes.models import Recipe
# from recipes.forms import RecipeForm, PostUpdateForm

# def show_recipe(request, id):
#     recipe = get_object_or_404(Recipe, id=id)
#     context = {
#         "recipe_object": recipe,
#     }
#     return render(request, "recipes/detail.html", context)

# def recipe_list(request):
#     recipes = Recipe.objects.all()
#     context = {
#         "recipe_list": recipes,
#     }
#     return render(request, "recipes/list.html", context)


# def create_recipe(request):
#     if request.method == "POST":
#         form = RecipeForm(request.POST)
#         if form.is_valid():
#             recipe = form.save(False)
#             recipe.author = request.user
#             form.save()
#             return redirect("recipe_list")
#     else:
#         form = RecipeForm()
#     context = {
#         "form": form,
#     }
#     return render(request, "recipes/create.html", context)

# def edit_post(request):
#     if request.method == "POST":
#         form = PostUpdateForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect("list_posts")
#     else:
#         form = PostUpdateForm()
#     context = {
#         "form": form
#     }
#     return render(request, "recipes/edit.html", context)
