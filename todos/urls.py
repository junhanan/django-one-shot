from django.urls import path
from todos.views import todo_list, todo_list_detail, todo_list_create, todo_list_update

urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("<int:id>/edit/", todo_list_update, name="todo_list_update")
]


# from django.urls import path
# from django.shortcuts import redirect
# from recipes.views import show_recipe, recipe_list, create_recipe, edit_post

# urlpatterns = [
#     path("recipes/", recipe_list, name="recipe_list"),
#     path("recipes/<int:id>/", show_recipe, name="show_recipe"),
#     path("recipes/create/", create_recipe, name="create_recipe"),
#     path("recipes/<int:pk>/edit/", edit_post, name="edit_post")
#     ]
