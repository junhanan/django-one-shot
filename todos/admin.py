from django.contrib import admin
from todos.models import TodoList, TodoItem
# Register your models here.

@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )

@admin.register(TodoItem)
class TodoItem(admin.ModelAdmin):
    list_display = (
        "task",
        "due_date",
        "is_completed",
        "list",
    )














# from django.contrib import admin
# from recipes.models import Recipe, RecipeStep, Ingredient

# # Register your models here.
# @admin.register(Recipe)
# class RecipeAdmin(admin.ModelAdmin):
#     list_display = (
#         "title",
#         "id",
#     )

# @admin.register(RecipeStep)
# class RecipeStepAdmin(admin.ModelAdmin):
#     list_display = (
#         "step_number",
#         "id",
#         "instruction"
#     )

# @admin.register(Ingredient)
# class RecipeAdmin(admin.ModelAdmin):
#     list_display = (
#         "amount",
#         "food_item"
#     )
